## Solid Bank App

<hr>

### Running:

    To run download jusan-spring-cli.jar and paste command: 

`java -jar jusan-spring-cli.jar`

<hr>

### Welcome to CLI bank service.

                Operations:
                1 - show accounts
                2 - create account
                3 - deposit
                4 - withdraw
                5 - transfer
                6 - this message
                7 - exit

![cli-running](https://ucarecdn.com/b25038ef-e370-46f7-aacb-3357cffa4138/)

<hr>

### UML diagram

#### The Unified Modeling Language (UML) is a general-purpose, developmental, modeling language in the field of software engineering that is intended to provide a standard way to visualize the design of a system.

![uml-diagram](https://ucarecdn.com/9c83ab15-eac1-4e21-85a2-ca2770fc2d90/)

<hr>

### Commits:
#### Commit types

| **Emoji** | **Commit type** |
|:---------:|:---------------:|
|    🔥     |  Major update   |
|    💧     |  Minor update   |
|    👾     |       Fix       |
|    🐣     |       Add       |
|    ✂     |    Refactor     |
|    🥂     |     Finish      |

<hr>
