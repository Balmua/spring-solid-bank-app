package kz.jusan.spring.bank.cli.jusanspringcli.cli;

import kz.jusan.spring.bank.cli.jusanspringcli.accountsType.AccountType;

public interface CreateAccountOperationUI {
    AccountType requestAccountType();

}
