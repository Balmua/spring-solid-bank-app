package kz.jusan.spring.bank.cli.jusanspringcli.accountsType;

public enum AccountType {
    CHECKING,
    SAVING,
    FIXED,
}
