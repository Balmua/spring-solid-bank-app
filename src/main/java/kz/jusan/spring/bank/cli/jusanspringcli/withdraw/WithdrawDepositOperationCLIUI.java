package kz.jusan.spring.bank.cli.jusanspringcli.withdraw;

public interface WithdrawDepositOperationCLIUI {
    double requestClientAmount();

    String requestClientAccountNumber();
}
